/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.LatLng;
import com.graphhopper.*;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.util.PointList;
import com.graphhopper.util.shapes.Circle;
import com.graphhopper.util.shapes.GHPoint;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.awt.Point;
import java.io.IOException;
import java.rmi.server.LogStream;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import java.sql.Timestamp;
/**
 *
 * @author Hazem
 */
public class Route {

    private int routeId;
    private double departure_latitude;
    private double departure_longitude;
    private double destination_latitude;
    private double destination_longitude;
    private Timestamp departure_time;
    private List<Coordinate> routePoints;
    private double bearing;
    private int availableSeats;
    private double routeDistance;//meter
    private long routeTime;//seconds
    
    public Route(double departureLatitude, double departureLongitude, double destinationLatitude, double destinationLongitude, String departureTime) throws ParseException {
        this.departure_latitude = departureLatitude;
        this.departure_longitude = departureLongitude;
        this.destination_latitude = destinationLatitude;
        this.destination_longitude = destinationLongitude;
        if(departureTime!=null)
        { 
            
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); 
            //sdf.setLenient(false);
            this.departure_time = Timestamp.valueOf(departureTime);
            System.out.println(this.departure_time.toString());
         }

        
        this.routePoints=new ArrayList<>();
        this.bearing=getHeadingForDirectionFromCoordinate();
    }
    
    public Route(){
        this.routePoints=new ArrayList<>();
    }
    public void setRouteId(int id){
        this.routeId=id;
    }
    
    public int getRouteId(){
        return this.routeId;
    }
    public double getDepLat(){
        return departure_latitude;
    }
    public void setDepLat(double value){
        this.departure_latitude=value;
    }
    
    public double getDepLon(){
        return departure_longitude;
    }
    public void setDepLon(double value){
        this.departure_longitude=value;
    }
    
    public double getDestLat(){
        return destination_latitude;
    }
    public void setDestLat(double value){
        this.destination_latitude=value;
    }
    
    public double getDestLon(){
        return destination_longitude;
    }
    public void setDestLon(double value){
        this.destination_longitude=value;
    }
    
    public Date getDepartureTme(){
        return departure_time;
    }
    public double getBearing(){
        return bearing;
    }
    public List<Coordinate> getRoutePoints(){
        return routePoints;
    }
    
    public void setRidePoints(List<Coordinate> points){
        this.routePoints=points;
    }

    public double getRouteDistance(){
        return this.routeDistance;
    }
    
    public long getRouteTime(){
        return this.routeTime;
    }
            
    public void createRoute() {
        if (this != null) {
            GHRequest req = new GHRequest(this.departure_latitude, this.departure_longitude, this.destination_latitude, this.destination_longitude).
                    setWeighting("fastest").
                    setVehicle("car");
            GraphHopper hopper=GraphHopperHelper.getHopper();
            GHResponse rsp = hopper.route(req);
            if(rsp.hasErrors())
            {
                throw new Error(rsp.getErrors().toString());
            }
            PointList points=rsp.getPoints();
            for (int i = 0; i < points.size(); i++) {
                this.routePoints.add(new Coordinate(points.getLatitude(i), points.getLongitude(i)));
            }
            this.routeDistance=rsp.getDistance();
            this.routeTime=rsp.getMillis()/1000;//millis to seconds
        } 
    }
    
    public void createRouteWithIntermediatePoints(List<Coordinate> intermediatePoints) {
        if (this != null) {
            List<GHPoint>newList=new ArrayList<>();
            for(Coordinate point:intermediatePoints)
            {
             newList.add(new GHPoint(point.getLat(), point.getLon()));   
            }
            GHRequest req = new GHRequest(newList).
                    setWeighting("fastest").
                    setVehicle("car");
            GraphHopper hopper=GraphHopperHelper.getHopper();
            GHResponse rsp = hopper.route(req);
            if(rsp.hasErrors())
            {
                throw new Error(rsp.getErrors().toString());
            }
            PointList points=rsp.getPoints();
            for (int i = 0; i < points.size(); i++) {
                this.routePoints.add(new Coordinate(points.getLatitude(i), points.getLongitude(i)));
            }
            if(this.routePoints.size()>2)
            {
                this.departure_latitude=this.routePoints.get(0).getLat();
                this.departure_longitude=this.routePoints.get(0).getLon();
                this.destination_latitude=this.routePoints.get(this.routePoints.size()-1).getLat();
                this.destination_longitude=this.routePoints.get(this.routePoints.size()-1).getLon();
                this.bearing=this.getHeadingForDirectionFromCoordinate();
                this.routeDistance=rsp.getDistance();
                this.routeTime=rsp.getMillis()/1000;//millis to seconds
            }
        } 
    }

    public void createRouteInDB() throws SQLException {
        // points, distance in meters and time in millis of the full path
        ResultSet maxRideId = SlimConnectionManager.selectCommand("SELECT MAX(PATH_ID) AS MAX_ID FROM RIDE");
        int rideId = 0;
        if (maxRideId.next()) {
            if(maxRideId.getString("MAX_ID")!=null)
                rideId = Integer.parseInt(maxRideId.getString("MAX_ID")) + 1;
            else rideId=1;
        }

        //set the ride information in RIDE table
        String query="insert into RIDE (PATH_ID,DEPT_LAT,DEPT_LON,DEST_LAT,DEST_LON,DEPT_TIME,BEARING,DURATION,DISTANCE) "
                + "values ("+rideId+","+departure_latitude+","+departure_longitude+","+destination_latitude+","+destination_longitude+",'"+
                "2015-01-01 00:00:00"+"',"+bearing+","+this.routeTime+","+this.routeDistance+ ")";
          SlimConnectionManager.executeCommand(query);
        //set ride points int RIDE_PATH table
        for (int i = 0; i < this.routePoints.size(); i++) {
            //get node from path route
            double longitude = this.routePoints.get(i).getLon();
            double latitude = this.routePoints.get(i).getLat();
            //create unique node in NODE table
            Node node = new Node(longitude, latitude);
            node.createNodeInDatabase();

            //create the route data in RIDE_PATH table
            SlimConnectionManager.executeCommand("insert into RIDE_PATH (PATH_ID,LONGITUDE,LATITUDE,NODE_ORDER) "
                    + "values (" + rideId + "," + String.valueOf(longitude) + "," + String.valueOf(latitude) + "," + String.valueOf(i) + ")");
        }
        insertNodePathMapping(this.routePoints, rideId);
        GraphHopperHelper.LOGGER.log(Level.INFO,"Ride created in database!");

    }

    /*
    This method aims to buid the mapping between list of Nodes and a ride
    */ 
    private void insertNodePathMapping(List<Coordinate> pointList, int pathId) throws SQLException {
        for (int i = 0; i < pointList.size(); i++) {
            double longitude = pointList.get(i).getLon();
            double latitude = pointList.get(i).getLat();
            double node_id = generateUniqueId(longitude, latitude);
            String query = "INSERT INTO PATH_MAPPING("
                    + "SELECT " + String.valueOf(node_id) + " as nodeid," + String.valueOf(pathId) + " as pathid"
                    + " FROM PATH_MAPPING"
                    + " WHERE"
                    + " NODE_ID = " + String.valueOf(node_id) + " AND PATH_ID =  " + String.valueOf(pathId) + " HAVING count(*)=0)";
            //System.out.println(query);
            SlimConnectionManager.executeCommand(query);
        }
    }

    public List<Point> getRoutePointsFromDB(int routeId) throws SQLException {
        ResultSet result = SlimConnectionManager.selectCommand("select * from RIDE_PATH where PATH_ID=" + String.valueOf(routeId));
        if (result.next()) {
                
        }
        return null;
    }

    private double generateUniqueId(double lon, double lat) {
        //pi(k1, k2) = 1/2(k1 + k2)(k1 + k2 + 1) + k2
        //ref: http://www.cs.upc.edu/~alvarez/calculabilitat/enumerabilitat.pdf
        double fn = 0.5 * (lon * lat) * (lon + lat + 1) + lat;
        return fn;

    }

    private double getHeadingForDirectionFromCoordinate() {
        double fromLat = degreesToRadians(departure_latitude);
        double fromLon = degreesToRadians(departure_longitude);
        double toLat = degreesToRadians(destination_latitude);
        double toLon = degreesToRadians(destination_longitude);

        double degree = radiandsToDegrees(Math.atan2(Math.sin(toLon-fromLon)*Math.cos(toLat), Math.cos(fromLat)*Math.sin(toLat)-Math.sin(fromLat)*Math.cos(toLat)*Math.cos(toLon-fromLon)));

        if (degree >= 0) {
            return degree;
        } else {
            return 360+degree;
        }
    }
    
    private double degreesToRadians(double x) {
        return Math.PI * x /180;
    }

    private double radiandsToDegrees(double x) {
        return x * 180.0 / Math.PI;
    }
    
    public void getRideFromDB(int rideId) throws SQLException
    {
       //TODO: select from RIDE to get ride bearing and dept time
        
       ResultSet result= SlimConnectionManager.selectCommand("select * from RIDE_PATH where PATH_ID="+String.valueOf(rideId));
       while(result.next())
       {
           double nodeOrder=result.getDouble("NODE_ORDER");
           double lat=result.getDouble("LATITUDE");
           double lon=result.getDouble("LONGITUDE");
       }
        
    }
    
    public boolean canJoindRide()
    {
        return false;
    }
    
    public void deleteRouteFromDB(int routeId) throws SQLException
    {

        //1. delete route in PATH table
        SlimConnectionManager.executeCommand("delete from RIDE where PATH_ID="+routeId);
        
        //2. delete route points from RIDE_PATH table
        SlimConnectionManager.executeCommand("delete from RIDE_PATH where PATH_ID="+routeId);
        
        //3. delete all nodes with points to this route in PATH_MAPPING table
        SlimConnectionManager.executeCommand("delete from PATH_MAPPING where PATH_ID="+routeId);
        
    }
    
    
}
