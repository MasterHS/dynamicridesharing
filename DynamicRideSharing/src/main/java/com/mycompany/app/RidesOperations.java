/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app;

import com.graphhopper.util.shapes.Circle;
import static java.lang.Math.abs;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import org.openstreetmap.gui.jmapviewer.Coordinate;

/**
 *
 * @author Hazem
 */
public class RidesOperations {
 
    
    public static void searchForRide(Route passengerRoute, double radius,double bearinAngle,String timestamp,int timeMargin) throws SQLException {

        Circle departureArea = new Circle(passengerRoute.getDepLat(), passengerRoute.getDepLon(), radius);
        Circle destinationArea = new Circle(passengerRoute.getDestLat(), passengerRoute.getDestLon(), radius);
        List<Integer> departurePoinAvaliablePaths = getRidesWithinCircularArea(departureArea);
        List<Integer> destinationPoinAvaliablePaths = getRidesWithinCircularArea(destinationArea);
            
        //intersect between the two lists and keep the intersection result in departurePoinAvaliablePaths
        departurePoinAvaliablePaths.retainAll(destinationPoinAvaliablePaths);
        for(Integer pathId:departurePoinAvaliablePaths)
        {
            double driverPathBearing=getRouteBearingFromDB(pathId);
            double difference;
            if(driverPathBearing==-1) return;
            
                if(abs(driverPathBearing-passengerRoute.getBearing())>180)
                {
                    if(driverPathBearing>passengerRoute.getBearing())
                    {
                        difference=360-driverPathBearing+passengerRoute.getBearing();
                    }
                    else
                    {
                        difference=360-passengerRoute.getBearing()+driverPathBearing;
                    }
                }
                else
                {
                    difference=abs(driverPathBearing-passengerRoute.getBearing());
                }
                if(difference>bearinAngle)
                {
                    GraphHopperHelper.LOGGER.log(Level.WARNING,"Can not join ride "+pathId+", bearing is greater than {0}",bearinAngle);
                    GraphHopperHelper.LOGGER.log(Level.INFO,"passenger bearing:"+passengerRoute.getBearing()+", ride bearing {0}",driverPathBearing);
                }
                else
                {
                    GraphHopperHelper.LOGGER.log(Level.INFO,"passenger can join ride "+pathId+", bearing is within {0}",bearinAngle);
                    GraphHopperHelper.LOGGER.log(Level.INFO,"passenger bearing:"+passengerRoute.getBearing()+", ride bearing {0}",driverPathBearing);
                }
            
            
        }
    }
    
    /**
     * Get all routes which intersect with circle
     * 
     * @param circle the area to search for routes inside
     * 
     */
    private static List<Integer> getRidesWithinCircularArea(Circle circle) throws SQLException {
        //get all the unique nodes in the system
        ResultSet uniqueSystemNodes = SlimConnectionManager.selectCommand("select * from NODE");
        List<Integer> pathIds = new ArrayList<>();
        while (uniqueSystemNodes.next()) {
            if (circle.contains(uniqueSystemNodes.getDouble("LATITUDE"), uniqueSystemNodes.getDouble("LONGITUDE"))) {
                double nodeId = uniqueSystemNodes.getDouble("NODE_ID");//generateUniqueId(uniqueSystemNodes.getDouble("LONGITUDE"), uniqueSystemNodes.getDouble("LATITUDE"));
                ResultSet paths = SlimConnectionManager.selectCommand("select PATH_ID from PATH_MAPPING where NODE_ID=" + nodeId);
                while (paths.next()) {
                    pathIds.add(paths.getInt("PATH_ID"));
                    
                }
            }
        }
        return pathIds;
    }
    
    public static void joinRide(Route passengerRoute, int driverRideId) throws SQLException, ParseException
    {
        //get ride info from database
        ResultSet rideinfo=SlimConnectionManager.selectCommand("select * from Ride where PATH_ID="+driverRideId);
        if(rideinfo.next())
        {
        double driverDeptLat=rideinfo.getDouble("DEPT_LAT");
        double driverDeptLon=rideinfo.getDouble("DEPT_LON");
        double driverDestLat=rideinfo.getDouble("DEST_LAT");
        double driverDestLon=rideinfo.getDouble("DEST_LON");
        //ride X--->Z
        //passenger a--b
        //new ride X-->a-->b-->Z
        
        //get the ride between X-->a
        List<Coordinate> points=new ArrayList<>();
        points.add(new Coordinate(driverDeptLat,driverDeptLon));
        points.add(new Coordinate(passengerRoute.getDepLat(),passengerRoute.getDepLon()));
        points.add(new Coordinate(passengerRoute.getDestLat(),passengerRoute.getDestLon()));
        points.add(new Coordinate(driverDestLat,driverDestLon));
        Route newRoute=new Route();
        newRoute.createRouteWithIntermediatePoints(points);

       //delete old driver route form database (path from X-->Z)
        passengerRoute.deleteRouteFromDB(driverRideId);
       //calculate the new distance and route time
        
        //create the new route in the database
        newRoute.createRouteInDB();
        
        }
    }
    
    public static double getRouteBearingFromDB(Integer pathId)
    {
           try
           {
            ResultSet result= SlimConnectionManager.selectCommand("select BEARING from RIDE where PATH_ID="+pathId);
            if(result.next())
            return result.getDouble(1);
            else return -1;
           }
           catch(SQLException ex)
           {
               GraphHopperHelper.LOGGER.log(Level.SEVERE,"Cannot get the bearing of ride id {0}",pathId);
           }
           return -1;
    }
    
}
