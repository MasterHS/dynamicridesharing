/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderGeometry;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.LatLng;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.search.Geocoding;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.util.shapes.GHPlace;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.List;
import java.util.logging.Level;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

/**
 *
 * @author Hazem
 */
public class GraphHopperHelper {

    private static GraphHopper hopper;
    public static Logger LOGGER = Logger.getLogger("InfoLogging");
    public static boolean loadHopper() {
        hopper = new GraphHopper().forServer();
        hopper.setInMemory();
        LOGGER.info("Setting map file location...");
        hopper.setOSMFile("../Map/bayern-latest.osm.pbf");
        // where to store graphhopper files?
        hopper.setGraphHopperLocation("../Map/bayern-latest.osm-gh2");
        hopper.setEncodingManager(new EncodingManager("car"));
        // now this can take minutes if it imports or a few seconds for loading
        LOGGER.info("importing map file...");
        hopper.importOrLoad();
        return true;
        // simple configuration of the request object, see the GraphHopperServlet classs for more possibilities.  
        
    }

    public static GraphHopper getHopper() {
        return hopper;
    }

    public static LatLng gecodeAddress(String address){

        LOGGER.log(Level.INFO, "getting geocode of address:{0}", address);
        final Geocoder geocoder = new Geocoder();
        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(address).setLanguage("de").getGeocoderRequest();
        LatLng geocode=null;
        try
        {
            GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
            List<GeocoderResult> results = geocoderResponse.getResults();
            geocode= results.get(0).getGeometry().getLocation();
        }
        catch(IOException ex)
        {
            LOGGER.log(Level.SEVERE,"Can not locate address:{0}",address);
        }
        return geocode;
    }

    public static List<Route> getRidesFromDatabase(){
        try
        {
        ResultSet rides = SlimConnectionManager.selectCommand("select * from RIDE_PATH order by PATH_ID,NODE_ORDER");
        Route ride = null;
        List<Coordinate> points = null;
        double prevLat = 0, prevLon = 0;
        List<Route> systemRides = new ArrayList<>();
        while (rides.next()) {
            if (rides.getInt("NODE_ORDER") == 0) {
                if (prevLat != 0 && prevLon != 0) {
                    ride.setDestLat( prevLat);
                    ride.setDestLon(prevLon);
                    ride.setRidePoints(points);
                    systemRides.add(ride);
                }

                ride = new Route();
                ride.setRouteId(rides.getInt("PATH_ID"));
                ride.setDepLat(rides.getDouble("LATITUDE"));
                ride.setDepLon(rides.getDouble("LONGITUDE"));
                points = new ArrayList<>();
                points.add(new Coordinate(rides.getDouble("LATITUDE"), rides.getDouble("LONGITUDE")));

            } else {
                if (rides.isLast()) {
                    ride.setDestLat(rides.getDouble("LATITUDE"));
                    ride.setDestLon(rides.getDouble("LONGITUDE"));
                    points.add(new Coordinate(rides.getDouble("LATITUDE"), rides.getDouble("LONGITUDE")));
                    ride.setRidePoints(points);
                    systemRides.add(ride);
                } else {
                    points.add(new Coordinate(rides.getDouble("LATITUDE"), rides.getDouble("LONGITUDE")));
                }
            }
            prevLat = rides.getDouble("LATITUDE");
            prevLon = rides.getDouble("LONGITUDE");
            
        }
        return systemRides;
        }
        catch(SQLException ex)
        {
            LOGGER.log(Level.SEVERE,"Unable to get rides from database");
        }
        return null;
    }

    //MapPolygon bermudas = new MapPolygonImpl(c(49,1), c(45,10), c(40,5));

//    public static List<MapPolygon> getRidesPolygons() {
//        try
//        {
//        List<Route> rides = getRidesFromDatabase();
//        List<MapPolygon> result = new ArrayList<>();
//        for (int i = 0; i < rides.size(); i++) {
//            MapPolygon polygon = new MapPolygonImpl(rides.get(i).getRoutePoints());
//            result.add(polygon);
//        }
//        return result;
//        }
//        catch(SQLException ex)
//        {
//            LOGGER.log(Level.SEVERE,"");
//        }
//    }
    

    

    

}
