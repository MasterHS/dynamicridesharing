/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hazem
 */
public class SlimConnectionManager {

    static Connection connectionManager;

    public static boolean connect() {
        try {
            String url = "jdbc:derby://localhost:1527/tumitfahrer_db";
            String username = "admin123";
            String password = "admin123";
            GraphHopperHelper.LOGGER.log(Level.INFO, "Connecting to tumitfahrer_db...");
            connectionManager = DriverManager.getConnection(url, username, password);
            GraphHopperHelper.LOGGER.log(Level.INFO, "Connected to tumitfahrer_db");
            return true;
        } catch (SQLException ex) {
            GraphHopperHelper.LOGGER.log(Level.SEVERE, "Unable to connect to database, please check port:1527");
        }
        return false;
    }

    public static void executeCommand(String query) {
        try {
            Statement stmt = SlimConnectionManager.connectionManager.createStatement();
            stmt.execute(query);
        } catch (SQLException ex) {
            GraphHopperHelper.LOGGER.log(Level.SEVERE, "Unable to execute command {0}", query);
        }
    }

    public static ResultSet selectCommand(String query){
        try {
            Statement stmt = SlimConnectionManager.connectionManager.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet results = stmt.executeQuery(query);
            return results;
        } catch (SQLException ex) {
            GraphHopperHelper.LOGGER.log(Level.SEVERE, "Unable to execute command {0}", query);
        }
        return null;
    }
}
