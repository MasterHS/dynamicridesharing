/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app;

import com.graphhopper.util.PointList;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Hazem
 */
public class Node {

    public double longitude;
    public double latitude;
    
    public Node(double longitude,double latitude)
    {
        this.longitude=longitude;
        this.latitude=latitude;
    }
    
    
    public void createNodeInDatabase() throws SQLException
    {
        double node_id=pairingfunction(this.longitude,this.latitude);
                     String query="INSERT INTO NODE("+
                    " SELECT "+String.valueOf(node_id)+ " as nodeid,"+String.valueOf(this.longitude)+" as lon,"+String.valueOf(this.latitude)+" as lat"+
                    " FROM NODE"+
                    " WHERE" +
                     " NODE_ID = "+String.valueOf(node_id) +" AND LONGITUDE =  "+String.valueOf(this.longitude) +" AND LATITUDE =  "+String.valueOf(this.latitude) +
                    " HAVING count(*)=0)";
                     //System.out.println(query);
        SlimConnectionManager.executeCommand(query); 
    }

     private double pairingfunction(double lon,double lat)
    {
        //pi(k1, k2) = 1/2(k1 + k2)(k1 + k2 + 1) + k2
        //ref: http://www.cs.upc.edu/~alvarez/calculabilitat/enumerabilitat.pdf
       double fn=0.5*(lon*lat)*(lon+lat+1)+lat;
       return fn;
        
    }
    
}
